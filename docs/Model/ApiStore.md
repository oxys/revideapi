# ApiStore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**country_code** | **string** |  | [optional] 
**county** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**adjacent_zip_codes** | **string** |  | [optional] 
**email_unsubscribe_text** | **string** |  | [optional] 
**email_view_online_text** | **string** |  | [optional] 
**external_id** | **string** |  | [optional] 
**footer_html** | **string** |  | [optional] 
**header_html** | **string** |  | [optional] 
**homepage** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**region** | **string** |  | [optional] 
**sender_address** | **string** |  | [optional] 
**sender_name** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**zip_code** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


