# MemberStatusModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_color** | **string** |  | [optional] 
**status_text** | **string** |  | [optional] 
**data** | **map[string,object]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


