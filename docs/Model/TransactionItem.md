# TransactionItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**transaction_number** | **string** |  | [optional] 
**created_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**store_name** | **string** |  | [optional] 
**number_of_items** | **int** |  | [optional] 
**net_price_sum** | **double** |  | [optional] 
**store_type** | **string** |  | [optional] 
**line_items** | [**map[string,object][]**](map.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


