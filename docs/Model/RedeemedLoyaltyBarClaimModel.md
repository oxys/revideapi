# RedeemedLoyaltyBarClaimModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**expire_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | **string** |  | [optional] 
**value** | **object** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


