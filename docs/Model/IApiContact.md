# IApiContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**attributes** | **map[string,object]** |  | [optional] 
**meta** | **map[string,object]** |  | [optional] 
**preferences** | **map[string,object]** |  | [optional] 
**consents** | [**\Swagger\Client\Model\ApiConsent[]**](ApiConsent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


