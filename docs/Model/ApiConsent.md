# ApiConsent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**value** | **bool** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**source** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


