# ApiContactOverviewSearchQueryData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_id** | **string** |  | [optional] 
**contact_type** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**social_security_number** | **string** |  | [optional] 
**mobile_phone** | **string** |  | [optional] 
**custom_key** | **string** |  | [optional] 
**any** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


