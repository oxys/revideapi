# ApiPromotionModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**expires_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**heading** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**redeemed** | **bool** |  | [optional] 
**redeemed_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**image_url** | **string** |  | [optional] 
**link** | **string** |  | [optional] 
**redemption_channels** | [**\Swagger\Client\Model\ApiPromotionRedemptionChannelModel[]**](ApiPromotionRedemptionChannelModel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


