# AllLoyaltyBarClaimModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redeemed_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**redeemed** | **bool** |  | [optional] 
**expires_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**expire_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | **string** |  | [optional] 
**value** | **object** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


