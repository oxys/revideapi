# RedeemBodyModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redemption_channel** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


