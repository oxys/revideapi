# SendSmsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **string** |  | [optional] 
**invoice_reference** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**phone_numbers** | **string[]** |  | [optional] 
**sender** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


