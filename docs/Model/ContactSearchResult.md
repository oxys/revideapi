# ContactSearchResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**care_of** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**zip_code** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**birth_day** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**phone_number** | **string** |  | [optional] 
**mobile_phone_number** | **string** |  | [optional] 
**gender** | **string** |  | [optional] 
**unregistered_from_online_register_information** | [**\Swagger\Client\Model\UnregisteredFromOnlineRegisterInformation**](UnregisteredFromOnlineRegisterInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


