# PagedResultAvailableLoyaltyBarClaimModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**offset** | **int** |  | [optional] 
**items** | [**\Swagger\Client\Model\AvailableLoyaltyBarClaimModel[]**](AvailableLoyaltyBarClaimModel.md) |  | [optional] 
**total_count** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


