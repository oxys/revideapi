# Swagger\Client\ConsentsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**consentsGetConsents**](ConsentsApi.md#consentsGetConsents) | **GET** /api/v2/consents | Get all consents definitions


# **consentsGetConsents**
> \Swagger\Client\Model\ApiConsentDefinition[] consentsGetConsents()

Get all consents definitions

Get all consents definitions    Example of metaData for a Consent:    \"metaData\": {    \"conditionText\": {      \"sv-SE\": \"Svensk villkorstext\",      \"en-GB\": \"English text to show for condition\"    },    \"displayText\": {      \"sv-SE\": \"Svensk text att visa\",      \"en-GB\": \"English text to display\"    },    \"linkText\": {      \"sv-SE\": \"Svensk text att visa på länk\",      \"en-GB\": \"English text to show on link\"    }  }

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ConsentsApi(new \Http\Adapter\Guzzle6\Client());

try {
    $result = $api_instance->consentsGetConsents();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConsentsApi->consentsGetConsents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\ApiConsentDefinition[]**](../Model/ApiConsentDefinition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

