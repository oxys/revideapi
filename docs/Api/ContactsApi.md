# Swagger\Client\ContactsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactPreferencesAcceptsEmail**](ContactsApi.md#contactPreferencesAcceptsEmail) | **POST** /api/v2/contacts/{contactId}/preferences/acceptsEmail | Update the accepts email preference for a contact.
[**contactPreferencesAcceptsPostal**](ContactsApi.md#contactPreferencesAcceptsPostal) | **POST** /api/v2/contacts/{contactId}/preferences/acceptsPostal | Update the accepts postal preference for a contact.
[**contactPreferencesAcceptsSms**](ContactsApi.md#contactPreferencesAcceptsSms) | **POST** /api/v2/contacts/{contactId}/preferences/acceptsSms | Update the accepts sms preference for a contact.
[**contactsV2Count**](ContactsApi.md#contactsV2Count) | **GET** /api/v2/contacts/count | Get number of approved contacts.
[**contactsV2Count_0**](ContactsApi.md#contactsV2Count_0) | **GET** /api/v2/contacts/{contactType}/count | Get number of approved contacts of given type.
[**contactsV2CreateContact**](ContactsApi.md#contactsV2CreateContact) | **POST** /api/v2/contacts | Create a contact.
[**contactsV2GetContactByExternalId**](ContactsApi.md#contactsV2GetContactByExternalId) | **GET** /api/v2/contacts/{contactType}/byexternalid/{externalId} | Get a single contact by type and external id.
[**contactsV2GetContactById**](ContactsApi.md#contactsV2GetContactById) | **GET** /api/v2/contacts/{contactId} | Get a single contact.
[**contactsV2GetContactByTypeAndKeyValue**](ContactsApi.md#contactsV2GetContactByTypeAndKeyValue) | **GET** /api/v2/contacts/{contactType}/bykey/{keyValue} | Get a single contact by type and key value.
[**contactsV2PromoteToMember**](ContactsApi.md#contactsV2PromoteToMember) | **POST** /api/v2/contacts/{contactId}/promoteToMember | Promotes a contact to a member.
[**contactsV2UpdateContact**](ContactsApi.md#contactsV2UpdateContact) | **POST** /api/v2/contacts/{contactId} | Update a contact.
[**transactionsGetTransactionsByContactId**](ContactsApi.md#transactionsGetTransactionsByContactId) | **GET** /api/v2/contacts/{contactId}/transactions | Get transactions for a single contact.


# **contactPreferencesAcceptsEmail**
> \Swagger\Client\Model\IApiContact contactPreferencesAcceptsEmail($contact_id, $accepts_email)

Update the accepts email preference for a contact.

Update the preference that indicates whether or not  a contact accepts to be contacted via email.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$accepts_email = new \Swagger\Client\Model\BoolRequest(); // \Swagger\Client\Model\BoolRequest | Preference value.

try {
    $result = $api_instance->contactPreferencesAcceptsEmail($contact_id, $accepts_email);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactPreferencesAcceptsEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **accepts_email** | [**\Swagger\Client\Model\BoolRequest**](../Model/BoolRequest.md)| Preference value. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactPreferencesAcceptsPostal**
> \Swagger\Client\Model\IApiContact contactPreferencesAcceptsPostal($contact_id, $accepts_postal)

Update the accepts postal preference for a contact.

Update the preference that indicates whether or not  a contact accepts to be contacted via regular mail.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$accepts_postal = new \Swagger\Client\Model\BoolRequest(); // \Swagger\Client\Model\BoolRequest | Preference value.

try {
    $result = $api_instance->contactPreferencesAcceptsPostal($contact_id, $accepts_postal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactPreferencesAcceptsPostal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **accepts_postal** | [**\Swagger\Client\Model\BoolRequest**](../Model/BoolRequest.md)| Preference value. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactPreferencesAcceptsSms**
> \Swagger\Client\Model\IApiContact contactPreferencesAcceptsSms($contact_id, $accepts_sms)

Update the accepts sms preference for a contact.

Update the preference that indicates whether or not  a contact accepts to be contacted via sms.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$accepts_sms = new \Swagger\Client\Model\BoolRequest(); // \Swagger\Client\Model\BoolRequest | Preference value.

try {
    $result = $api_instance->contactPreferencesAcceptsSms($contact_id, $accepts_sms);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactPreferencesAcceptsSms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **accepts_sms** | [**\Swagger\Client\Model\BoolRequest**](../Model/BoolRequest.md)| Preference value. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2Count**
> int contactsV2Count()

Get number of approved contacts.

Get numer of approved contacts.     This is a cached value that will be updated with a   set frequency (normally once every 20 min).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());

try {
    $result = $api_instance->contactsV2Count();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2Count: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2Count_0**
> int contactsV2Count_0($contact_type)

Get number of approved contacts of given type.

Get number of approved contacts of given type.     This is a cached value that will be updated with a   set frequency (normally once every 20 min).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_type = "contact_type_example"; // string | Id for contact type, e.g. \"member\" or \"contact\"

try {
    $result = $api_instance->contactsV2Count_0($contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2Count_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_type** | **string**| Id for contact type, e.g. \&quot;member\&quot; or \&quot;contact\&quot; |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2CreateContact**
> \Swagger\Client\Model\IApiContact contactsV2CreateContact($value, $source, $store_external_id, $create_as_unapproved)

Create a contact.

Create a new, approved contact.    The ingoing data must be a subset of the attributes data field  of a contact in the current instance configuration. For a list  of supported fields, just request a contact and see  which fields are available in the attributes data field.    Note: For Opt-In/Opt-out the preferences API-operations must be used:   acceptsEmail, acceptsPostal and acceptsSMS

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$value = new \stdClass; // object | Contact data.
$source = "source_example"; // string | Source system identifier (instance configuration)
$store_external_id = "store_external_id_example"; // string | Current store and recruited by store.
$create_as_unapproved = "create_as_unapproved_example"; // string | Contact is not approved on creation. (Default value false)

try {
    $result = $api_instance->contactsV2CreateContact($value, $source, $store_external_id, $create_as_unapproved);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2CreateContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | **object**| Contact data. |
 **source** | **string**| Source system identifier (instance configuration) | [optional]
 **store_external_id** | **string**| Current store and recruited by store. | [optional]
 **create_as_unapproved** | **string**| Contact is not approved on creation. (Default value false) | [optional]

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2GetContactByExternalId**
> \Swagger\Client\Model\IApiContact contactsV2GetContactByExternalId($contact_type, $external_id)

Get a single contact by type and external id.

Get a single contact of a certain type, using the  contact's external id.    The dynamic fields of the response object depend on  the current configuration.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_type = "contact_type_example"; // string | Contact type, e.g. Member or Contact.
$external_id = "external_id_example"; // string | External contact id.

try {
    $result = $api_instance->contactsV2GetContactByExternalId($contact_type, $external_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2GetContactByExternalId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_type** | **string**| Contact type, e.g. Member or Contact. |
 **external_id** | **string**| External contact id. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2GetContactById**
> \Swagger\Client\Model\IApiContact contactsV2GetContactById($contact_id)

Get a single contact.

Get a single contact, using the unique identifier.    The dynamic fields of the response object depend on  the current instance configuration.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).

try {
    $result = $api_instance->contactsV2GetContactById($contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2GetContactById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2GetContactByTypeAndKeyValue**
> \Swagger\Client\Model\IApiContact contactsV2GetContactByTypeAndKeyValue($contact_type, $key_value)

Get a single contact by type and key value.

Get a single contact of a certain type, using a key  value that corresponds to the current instance configuration. This can  only be used for contact types with exactly ONE key.    The dynamic fields of the response object depend on  the current configuration.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\".
$key_value = "key_value_example"; // string | Key value, e.g. ssn, phone number etc.

try {
    $result = $api_instance->contactsV2GetContactByTypeAndKeyValue($contact_type, $key_value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2GetContactByTypeAndKeyValue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot;. |
 **key_value** | **string**| Key value, e.g. ssn, phone number etc. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2PromoteToMember**
> \Swagger\Client\Model\IApiContact contactsV2PromoteToMember($contact_id, $value, $source)

Promotes a contact to a member.

Promote a contact to a member with one or several required fields.    The ingoing data must be a subset of the attributes data field  of a contact in the current instance configuration. For a list  of supported fields, just request a contact and see  which fields are available in the attributes data field.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$value = new \stdClass; // object | Updates to apply.
$source = "source_example"; // string | Source system identifier (instance configuration)

try {
    $result = $api_instance->contactsV2PromoteToMember($contact_id, $value, $source);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2PromoteToMember: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **value** | **object**| Updates to apply. |
 **source** | **string**| Source system identifier (instance configuration) | [optional]

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactsV2UpdateContact**
> \Swagger\Client\Model\IApiContact contactsV2UpdateContact($contact_id, $value)

Update a contact.

Update one or several fields of a single contact.    The ingoing data must be a subset of the attributes data field  of a contact in the current instance configuration. For a list  of supported fields, just request a contact and see  which fields are available in the attributes data field.    Note when updating currentStore: When this value is set it will  also disable current store calculation based on rfm.            Note: For Opt-In/Opt-out the preferences API-operations must be used:   acceptsEmail, acceptsPostal and acceptsSMS

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$value = new \stdClass; // object | Updates to apply.

try {
    $result = $api_instance->contactsV2UpdateContact($contact_id, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contactsV2UpdateContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **value** | **object**| Updates to apply. |

### Return type

[**\Swagger\Client\Model\IApiContact**](../Model/IApiContact.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionsGetTransactionsByContactId**
> \Swagger\Client\Model\PagedResultTransactionItem transactionsGetTransactionsByContactId($contact_id, $offset, $count)

Get transactions for a single contact.

Get all purchase transactions for a single contact with   optional offset and number of transactions in response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID)
$offset = 56; // int | Number of items to skip. (Default value 0)
$count = 56; // int | Max number of items to take. (Default value 100)

try {
    $result = $api_instance->transactionsGetTransactionsByContactId($contact_id, $offset, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->transactionsGetTransactionsByContactId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID) |
 **offset** | **int**| Number of items to skip. (Default value 0) | [optional]
 **count** | **int**| Max number of items to take. (Default value 100) | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultTransactionItem**](../Model/PagedResultTransactionItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

