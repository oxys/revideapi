# Swagger\Client\PromotionsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**offerPromotionsGetPromotionsForContact**](PromotionsApi.md#offerPromotionsGetPromotionsForContact) | **GET** /api/v2/contacts/{contactId}/promotions | Get available promotions for a contact
[**offerPromotionsRedeem**](PromotionsApi.md#offerPromotionsRedeem) | **POST** /api/v2/contacts/{contactId}/promotions/{id}/redeem | Redeem a promotion


# **offerPromotionsGetPromotionsForContact**
> \Swagger\Client\Model\ApiPromotionModel[] offerPromotionsGetPromotionsForContact($contact_id, $redemption_channel_type)

Get available promotions for a contact

Get available promotions for a contact. To filter on redemptionChannelType add it as a query string  ?redemptionChannelType=POS  It can be POS, ECOM or OTHER

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PromotionsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier
$redemption_channel_type = "redemption_channel_type_example"; // string | Filter on redemptionChannelType it can be POS, ECOM or OTHER

try {
    $result = $api_instance->offerPromotionsGetPromotionsForContact($contact_id, $redemption_channel_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromotionsApi->offerPromotionsGetPromotionsForContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier |
 **redemption_channel_type** | **string**| Filter on redemptionChannelType it can be POS, ECOM or OTHER | [optional]

### Return type

[**\Swagger\Client\Model\ApiPromotionModel[]**](../Model/ApiPromotionModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **offerPromotionsRedeem**
> object offerPromotionsRedeem($contact_id, $id, $value)

Redeem a promotion

Redeem a promotion (multichannel offer or mobile swipe) for a Contact using the internal Contact Id    Redemption channel can be POS, ECOM or OTHER.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PromotionsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier
$id = "id_example"; // string | The id of the promotion
$value = new \Swagger\Client\Model\RedeemBodyModel(); // \Swagger\Client\Model\RedeemBodyModel | The channel where the promotion was used

try {
    $result = $api_instance->offerPromotionsRedeem($contact_id, $id, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromotionsApi->offerPromotionsRedeem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier |
 **id** | **string**| The id of the promotion |
 **value** | [**\Swagger\Client\Model\RedeemBodyModel**](../Model/RedeemBodyModel.md)| The channel where the promotion was used |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

