# Swagger\Client\PersonlookupApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**personLookupGetPersonLookup**](PersonlookupApi.md#personLookupGetPersonLookup) | **GET** /api/v2/personlookup/getpersonlookup | mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.


# **personLookupGetPersonLookup**
> \Swagger\Client\Model\ContactSearchResult personLookupGetPersonLookup($social_security_number, $phone_number)

mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PersonlookupApi(new \Http\Adapter\Guzzle6\Client());
$social_security_number = "social_security_number_example"; // string | String that contains socialsecuritynumber.
$phone_number = "phone_number_example"; // string | String that contains phoneNumber.

try {
    $result = $api_instance->personLookupGetPersonLookup($social_security_number, $phone_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonlookupApi->personLookupGetPersonLookup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **social_security_number** | **string**| String that contains socialsecuritynumber. | [optional]
 **phone_number** | **string**| String that contains phoneNumber. | [optional]

### Return type

[**\Swagger\Client\Model\ContactSearchResult**](../Model/ContactSearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

