# Swagger\Client\AchievementsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**achievementsGetAchievementsForContact**](AchievementsApi.md#achievementsGetAchievementsForContact) | **GET** /api/v2/contacts/{contactId}/achievements | Get all achievements for a contact
[**achievementsGetAllAchievements**](AchievementsApi.md#achievementsGetAllAchievements) | **GET** /api/v2/achievements | Get definitions of all type of achievements
[**achievementsRemoveAchievement**](AchievementsApi.md#achievementsRemoveAchievement) | **DELETE** /api/v2/contacts/{contactId}/achievements/{achievementId} | Delete an achievement
[**achievementsSetAchievement**](AchievementsApi.md#achievementsSetAchievement) | **POST** /api/v2/contacts/{contactId}/achievements/{achievementId} | Sets an achievement with date for a contact  If contact already has the achievement the posted date is ignored


# **achievementsGetAchievementsForContact**
> \Swagger\Client\Model\ApiAchievementValue[] achievementsGetAchievementsForContact($contact_id)

Get all achievements for a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AchievementsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID)

try {
    $result = $api_instance->achievementsGetAchievementsForContact($contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AchievementsApi->achievementsGetAchievementsForContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID) |

### Return type

[**\Swagger\Client\Model\ApiAchievementValue[]**](../Model/ApiAchievementValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **achievementsGetAllAchievements**
> \Swagger\Client\Model\PagedResultApiAchievementDefinition achievementsGetAllAchievements($offset, $count)

Get definitions of all type of achievements

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AchievementsApi(new \Http\Adapter\Guzzle6\Client());
$offset = 56; // int | The index of the item to start from. (Default value 0)
$count = 56; // int | Max number of items returned. (Default value 100)

try {
    $result = $api_instance->achievementsGetAllAchievements($offset, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AchievementsApi->achievementsGetAllAchievements: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| The index of the item to start from. (Default value 0) | [optional]
 **count** | **int**| Max number of items returned. (Default value 100) | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultApiAchievementDefinition**](../Model/PagedResultApiAchievementDefinition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **achievementsRemoveAchievement**
> achievementsRemoveAchievement($contact_id, $achievement_id)

Delete an achievement

Deletes an achievement for a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AchievementsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID)
$achievement_id = "achievement_id_example"; // string | The id for this achievement

try {
    $api_instance->achievementsRemoveAchievement($contact_id, $achievement_id);
} catch (Exception $e) {
    echo 'Exception when calling AchievementsApi->achievementsRemoveAchievement: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID) |
 **achievement_id** | **string**| The id for this achievement |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **achievementsSetAchievement**
> achievementsSetAchievement($contact_id, $achievement_id, $value)

Sets an achievement with date for a contact  If contact already has the achievement the posted date is ignored

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AchievementsApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID)
$achievement_id = "achievement_id_example"; // string | The id of the achievement
$value = new \stdClass; // object | Date of Achievement in format {\"date\": \"2017-11-24\"}

try {
    $api_instance->achievementsSetAchievement($contact_id, $achievement_id, $value);
} catch (Exception $e) {
    echo 'Exception when calling AchievementsApi->achievementsSetAchievement: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID) |
 **achievement_id** | **string**| The id of the achievement |
 **value** | **object**| Date of Achievement in format {\&quot;date\&quot;: \&quot;2017-11-24\&quot;} |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

