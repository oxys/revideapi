# Swagger\Client\TransactionsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**importTransactionsImport**](TransactionsApi.md#importTransactionsImport) | **POST** /api/v2/transactions | 


# **importTransactionsImport**
> object importTransactionsImport($obj)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TransactionsApi(new \Http\Adapter\Guzzle6\Client());
$obj = new \Swagger\Client\Model\ImportTransactionsObject(); // \Swagger\Client\Model\ImportTransactionsObject | 

try {
    $result = $api_instance->importTransactionsImport($obj);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionsApi->importTransactionsImport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **obj** | [**\Swagger\Client\Model\ImportTransactionsObject**](../Model/ImportTransactionsObject.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

