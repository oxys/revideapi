# Swagger\Client\ContactoverviewApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactOverviewGetContactOverview**](ContactoverviewApi.md#contactOverviewGetContactOverview) | **GET** /api/v2/contactoverview | Get all information about a single contact.


# **contactOverviewGetContactOverview**
> map[string,object] contactOverviewGetContactOverview($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any)

Get all information about a single contact.

Get all information about a single contact by specifying either:  - contactId  - email and contactType  - socialSecurityNumber and contactType  - mobilePhone and contactType  - customKey and contactType (the customKey must be configured by your supplier)  - any and contactType - the any field can contain email, socialSecurityNumber, mobilePhone or the custom key (and are checked in that order)                The dynamic fields of the response depend on your current Voyado configuration.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContactoverviewApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | 
$contact_type = "contact_type_example"; // string | 
$email = "email_example"; // string | 
$social_security_number = "social_security_number_example"; // string | 
$mobile_phone = "mobile_phone_example"; // string | 
$custom_key = "custom_key_example"; // string | 
$any = "any_example"; // string | 

try {
    $result = $api_instance->contactOverviewGetContactOverview($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactoverviewApi->contactOverviewGetContactOverview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**|  | [optional]
 **contact_type** | **string**|  | [optional]
 **email** | **string**|  | [optional]
 **social_security_number** | **string**|  | [optional]
 **mobile_phone** | **string**|  | [optional]
 **custom_key** | **string**|  | [optional]
 **any** | **string**|  | [optional]

### Return type

**map[string,object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

