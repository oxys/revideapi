# Swagger\Client\AutomationApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customTriggersTriggerByContactId**](AutomationApi.md#customTriggersTriggerByContactId) | **POST** /api/v2/automation/customTriggers/{triggerId}/triggerByContactId/{contactId} | 
[**customTriggersTriggerByContactTypeAndKey**](AutomationApi.md#customTriggersTriggerByContactTypeAndKey) | **POST** /api/v2/automation/customTriggers/{triggerId}/triggerByContactTypeAndKey/{contactType}/{keyValue} | 
[**customTriggersTriggerByExternalContactId**](AutomationApi.md#customTriggersTriggerByExternalContactId) | **POST** /api/v2/automation/customTriggers/{triggerId}/triggerByExternalContactId/{externalId} | 
[**customTriggersTriggerBySocialSecurityNumber**](AutomationApi.md#customTriggersTriggerBySocialSecurityNumber) | **POST** /api/v2/automation/customTriggers/{triggerId}/triggerBySocialSecurityNumber/{ssn} | 


# **customTriggersTriggerByContactId**
> object customTriggersTriggerByContactId($trigger_id, $contact_id, $value)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AutomationApi(new \Http\Adapter\Guzzle6\Client());
$trigger_id = "trigger_id_example"; // string | 
$contact_id = "contact_id_example"; // string | 
$value = new \stdClass; // object | 

try {
    $result = $api_instance->customTriggersTriggerByContactId($trigger_id, $contact_id, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AutomationApi->customTriggersTriggerByContactId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trigger_id** | **string**|  |
 **contact_id** | **string**|  |
 **value** | **object**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customTriggersTriggerByContactTypeAndKey**
> object customTriggersTriggerByContactTypeAndKey($trigger_id, $contact_type, $key_value, $value)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AutomationApi(new \Http\Adapter\Guzzle6\Client());
$trigger_id = "trigger_id_example"; // string | 
$contact_type = "contact_type_example"; // string | 
$key_value = "key_value_example"; // string | 
$value = new \stdClass; // object | 

try {
    $result = $api_instance->customTriggersTriggerByContactTypeAndKey($trigger_id, $contact_type, $key_value, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AutomationApi->customTriggersTriggerByContactTypeAndKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trigger_id** | **string**|  |
 **contact_type** | **string**|  |
 **key_value** | **string**|  |
 **value** | **object**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customTriggersTriggerByExternalContactId**
> object customTriggersTriggerByExternalContactId($trigger_id, $external_id, $value)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AutomationApi(new \Http\Adapter\Guzzle6\Client());
$trigger_id = "trigger_id_example"; // string | 
$external_id = "external_id_example"; // string | 
$value = new \stdClass; // object | 

try {
    $result = $api_instance->customTriggersTriggerByExternalContactId($trigger_id, $external_id, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AutomationApi->customTriggersTriggerByExternalContactId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trigger_id** | **string**|  |
 **external_id** | **string**|  |
 **value** | **object**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customTriggersTriggerBySocialSecurityNumber**
> object customTriggersTriggerBySocialSecurityNumber($trigger_id, $ssn, $value)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AutomationApi(new \Http\Adapter\Guzzle6\Client());
$trigger_id = "trigger_id_example"; // string | 
$ssn = "ssn_example"; // string | 
$value = new \stdClass; // object | 

try {
    $result = $api_instance->customTriggersTriggerBySocialSecurityNumber($trigger_id, $ssn, $value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AutomationApi->customTriggersTriggerBySocialSecurityNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trigger_id** | **string**|  |
 **ssn** | **string**|  |
 **value** | **object**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

