# Swagger\Client\SmsApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**smsMessagesSendSmsToPhoneNumbers**](SmsApi.md#smsMessagesSendSmsToPhoneNumbers) | **POST** /api/v2/sms/sendToPhoneNumbers | Send sms to multiple phone numbers.


# **smsMessagesSendSmsToPhoneNumbers**
> \Swagger\Client\Model\SendSmsResponse smsMessagesSendSmsToPhoneNumbers($request)

Send sms to multiple phone numbers.

Send an sms to one or multiple phone numbers.     Phone numbers of a different nationality than that of the  tenant must be prefixed with a + or 00 (e.g. +4670xxxxxxx  or 004670xxxxxxx for a Swedish mobile phone number when the  tenant is non-Swedish).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SmsApi(new \Http\Adapter\Guzzle6\Client());
$request = new \Swagger\Client\Model\SendSmsRequest(); // \Swagger\Client\Model\SendSmsRequest | Message data.

try {
    $result = $api_instance->smsMessagesSendSmsToPhoneNumbers($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SmsApi->smsMessagesSendSmsToPhoneNumbers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Swagger\Client\Model\SendSmsRequest**](../Model/SendSmsRequest.md)| Message data. |

### Return type

[**\Swagger\Client\Model\SendSmsResponse**](../Model/SendSmsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

