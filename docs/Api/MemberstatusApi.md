# Swagger\Client\MemberstatusApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**memberstatusV2Get**](MemberstatusApi.md#memberstatusV2Get) | **GET** /api/v2/memberstatus | 


# **memberstatusV2Get**
> \Swagger\Client\Model\MemberStatusModel memberstatusV2Get($query)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\MemberstatusApi(new \Http\Adapter\Guzzle6\Client());
$query = "query_example"; // string | 

try {
    $result = $api_instance->memberstatusV2Get($query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MemberstatusApi->memberstatusV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **string**|  |

### Return type

[**\Swagger\Client\Model\MemberStatusModel**](../Model/MemberStatusModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

