# Swagger\Client\StoresApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**storesV2CreateStore**](StoresApi.md#storesV2CreateStore) | **POST** /api/v2/stores | Creates a store.
[**storesV2GetStore**](StoresApi.md#storesV2GetStore) | **GET** /api/v2/stores/{externalId} | Gets a single store.
[**storesV2GetStores**](StoresApi.md#storesV2GetStores) | **GET** /api/v2/stores | Gets all the stores.
[**storesV2UpdateStore**](StoresApi.md#storesV2UpdateStore) | **POST** /api/v2/stores/{externalId} | Updates a store.


# **storesV2CreateStore**
> \Swagger\Client\Model\ApiStore storesV2CreateStore($api_store)

Creates a store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StoresApi(new \Http\Adapter\Guzzle6\Client());
$api_store = new \Swagger\Client\Model\ApiStore(); // \Swagger\Client\Model\ApiStore | The store object to create.

try {
    $result = $api_instance->storesV2CreateStore($api_store);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->storesV2CreateStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_store** | [**\Swagger\Client\Model\ApiStore**](../Model/ApiStore.md)| The store object to create. |

### Return type

[**\Swagger\Client\Model\ApiStore**](../Model/ApiStore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storesV2GetStore**
> \Swagger\Client\Model\ApiStore storesV2GetStore($external_id, $include_inactive)

Gets a single store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StoresApi(new \Http\Adapter\Guzzle6\Client());
$external_id = "external_id_example"; // string | The external id of the store to get.
$include_inactive = true; // bool | Value indicating if the store can be inactive or not. (Default value = false)

try {
    $result = $api_instance->storesV2GetStore($external_id, $include_inactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->storesV2GetStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_id** | **string**| The external id of the store to get. |
 **include_inactive** | **bool**| Value indicating if the store can be inactive or not. (Default value &#x3D; false) | [optional]

### Return type

[**\Swagger\Client\Model\ApiStore**](../Model/ApiStore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storesV2GetStores**
> \Swagger\Client\Model\ApiStore[] storesV2GetStores($include_inactive)

Gets all the stores.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StoresApi(new \Http\Adapter\Guzzle6\Client());
$include_inactive = true; // bool | Value indicating if the inactive stores should be included or not. (Default value = false)

try {
    $result = $api_instance->storesV2GetStores($include_inactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->storesV2GetStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_inactive** | **bool**| Value indicating if the inactive stores should be included or not. (Default value &#x3D; false) | [optional]

### Return type

[**\Swagger\Client\Model\ApiStore[]**](../Model/ApiStore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **storesV2UpdateStore**
> \Swagger\Client\Model\ApiStore storesV2UpdateStore($external_id, $api_store)

Updates a store.

Updates a store. externalId is the store identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StoresApi(new \Http\Adapter\Guzzle6\Client());
$external_id = "external_id_example"; // string | The external id of the store to update.
$api_store = new \Swagger\Client\Model\ApiStore(); // \Swagger\Client\Model\ApiStore | The store object to update.

try {
    $result = $api_instance->storesV2UpdateStore($external_id, $api_store);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->storesV2UpdateStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **external_id** | **string**| The external id of the store to update. |
 **api_store** | [**\Swagger\Client\Model\ApiStore**](../Model/ApiStore.md)| The store object to update. |

### Return type

[**\Swagger\Client\Model\ApiStore**](../Model/ApiStore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

