# Swagger\Client\PosoffersApi

All URIs are relative to *https://colorama.staging.voyado.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**posOfferGetAllPosOffersByContactTypeAndKey**](PosoffersApi.md#posOfferGetAllPosOffersByContactTypeAndKey) | **GET** /api/v2/contacts/bykey/{keyValue}/posoffers/all | Get all POS offers for a contact
[**posOfferGetAllPosOffersByContactTypeAndKey_0**](PosoffersApi.md#posOfferGetAllPosOffersByContactTypeAndKey_0) | **GET** /api/v2/contacts/{contactType}/bykey/{keyValue}/posoffers/all | Get all POS offers for a contact
[**posOfferGetAllPosOffersForContact**](PosoffersApi.md#posOfferGetAllPosOffersForContact) | **GET** /api/v2/contacts/{contactId}/posoffers/all | Get all POS offers for a contact
[**posOfferGetAvailablePosOffersByContactTypeAndKey**](PosoffersApi.md#posOfferGetAvailablePosOffersByContactTypeAndKey) | **GET** /api/v2/contacts/bykey/{keyValue}/posoffers/available | Get all available POS offers for a contact
[**posOfferGetAvailablePosOffersByContactTypeAndKey_0**](PosoffersApi.md#posOfferGetAvailablePosOffersByContactTypeAndKey_0) | **GET** /api/v2/contacts/{contactType}/bykey/{keyValue}/posoffers/available | Get all available POS offers for a contact
[**posOfferGetAvailablePosOffersForContact**](PosoffersApi.md#posOfferGetAvailablePosOffersForContact) | **GET** /api/v2/contacts/{contactId}/posoffers/available | Get all available POS offers for a contact
[**posOfferRedeem**](PosoffersApi.md#posOfferRedeem) | **POST** /api/v2/contacts/{contactId}/posoffers/{id}/redeem | Redeems a POS offer
[**posOfferRedeemByContactTypeAndKey**](PosoffersApi.md#posOfferRedeemByContactTypeAndKey) | **POST** /api/v2/contacts/bykey/{keyValue}/posoffers/{id}/redeem | Redeems a POS offer
[**posOfferRedeemByContactTypeAndKey_0**](PosoffersApi.md#posOfferRedeemByContactTypeAndKey_0) | **POST** /api/v2/contacts/{contactType}/bykey/{keyValue}/posoffers/{id}/redeem | Redeems a POS offer


# **posOfferGetAllPosOffersByContactTypeAndKey**
> \Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel posOfferGetAllPosOffersByContactTypeAndKey($key_value, $contact_type)

Get all POS offers for a contact

Get all POS offers for a contact. Expired, redeemed and available.    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\".

try {
    $result = $api_instance->posOfferGetAllPosOffersByContactTypeAndKey($key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAllPosOffersByContactTypeAndKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot;. | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel**](../Model/PagedResultAllLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferGetAllPosOffersByContactTypeAndKey_0**
> \Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel posOfferGetAllPosOffersByContactTypeAndKey_0($key_value, $contact_type)

Get all POS offers for a contact

Get all POS offers for a contact. Expired, redeemed and available.    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\".

try {
    $result = $api_instance->posOfferGetAllPosOffersByContactTypeAndKey_0($key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAllPosOffersByContactTypeAndKey_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot;. |

### Return type

[**\Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel**](../Model/PagedResultAllLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferGetAllPosOffersForContact**
> \Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel posOfferGetAllPosOffersForContact($contact_id, $offset, $count)

Get all POS offers for a contact

Get all POS offers for a contact. Expired, redeemed and available.  The result can be paginated, using the offset and  count query parameters.  Note: *expiresOn* is obsolete and is always **null**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$offset = 56; // int | The first item to retrieve. (Default value 0)
$count = 56; // int | The max number of items to retrieve. (Default value 100)

try {
    $result = $api_instance->posOfferGetAllPosOffersForContact($contact_id, $offset, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAllPosOffersForContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **offset** | **int**| The first item to retrieve. (Default value 0) | [optional]
 **count** | **int**| The max number of items to retrieve. (Default value 100) | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultAllLoyaltyBarClaimModel**](../Model/PagedResultAllLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferGetAvailablePosOffersByContactTypeAndKey**
> \Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel posOfferGetAvailablePosOffersByContactTypeAndKey($key_value, $contact_type)

Get all available POS offers for a contact

Get all available POS offers for a contact.   Expired and redeemed offers are excluded.    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\".

try {
    $result = $api_instance->posOfferGetAvailablePosOffersByContactTypeAndKey($key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAvailablePosOffersByContactTypeAndKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot;. | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel**](../Model/PagedResultAvailableLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferGetAvailablePosOffersByContactTypeAndKey_0**
> \Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel posOfferGetAvailablePosOffersByContactTypeAndKey_0($key_value, $contact_type)

Get all available POS offers for a contact

Get all available POS offers for a contact.   Expired and redeemed offers are excluded.    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\".

try {
    $result = $api_instance->posOfferGetAvailablePosOffersByContactTypeAndKey_0($key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAvailablePosOffersByContactTypeAndKey_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot;. |

### Return type

[**\Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel**](../Model/PagedResultAvailableLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferGetAvailablePosOffersForContact**
> \Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel posOfferGetAvailablePosOffersForContact($contact_id, $offset, $count)

Get all available POS offers for a contact

Get all available POS offers for a contact.   Expired and redeemed offers are excluded.    The result can be paginated, using the offset and  count query parameters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).
$offset = 56; // int | The first item to retrieve. (Default value 0)
$count = 56; // int | The max number of items to retrieve. (Default value 100)

try {
    $result = $api_instance->posOfferGetAvailablePosOffersForContact($contact_id, $offset, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferGetAvailablePosOffersForContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **string**| Contact identifier (GUID). |
 **offset** | **int**| The first item to retrieve. (Default value 0) | [optional]
 **count** | **int**| The max number of items to retrieve. (Default value 100) | [optional]

### Return type

[**\Swagger\Client\Model\PagedResultAvailableLoyaltyBarClaimModel**](../Model/PagedResultAvailableLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferRedeem**
> \Swagger\Client\Model\RedeemedLoyaltyBarClaimModel posOfferRedeem($id, $contact_id)

Redeems a POS offer

Redeems a POS offer for a Contact using the internal Contact Id

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$id = "id_example"; // string | The id returned from the get operation (GUID)
$contact_id = "contact_id_example"; // string | Contact identifier (GUID).

try {
    $result = $api_instance->posOfferRedeem($id, $contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferRedeem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The id returned from the get operation (GUID) |
 **contact_id** | **string**| Contact identifier (GUID). |

### Return type

[**\Swagger\Client\Model\RedeemedLoyaltyBarClaimModel**](../Model/RedeemedLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferRedeemByContactTypeAndKey**
> \Swagger\Client\Model\RedeemedLoyaltyBarClaimModel posOfferRedeemByContactTypeAndKey($id, $key_value, $contact_type)

Redeems a POS offer

Redeems a POS offer for a Contact using the key for the contact type    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$id = "id_example"; // string | The id returned from the get operation (GUID)
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\" or \"contact\".

try {
    $result = $api_instance->posOfferRedeemByContactTypeAndKey($id, $key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferRedeemByContactTypeAndKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The id returned from the get operation (GUID) |
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot; or \&quot;contact\&quot;. | [optional]

### Return type

[**\Swagger\Client\Model\RedeemedLoyaltyBarClaimModel**](../Model/RedeemedLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **posOfferRedeemByContactTypeAndKey_0**
> \Swagger\Client\Model\RedeemedLoyaltyBarClaimModel posOfferRedeemByContactTypeAndKey_0($id, $key_value, $contact_type)

Redeems a POS offer

Redeems a POS offer for a Contact using the key for the contact type    Finds the contact by using a key value other than Contact Id. This can  only be used for contact types with exactly ONE key.  The contact key attribute is configured for each Voyado instance.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\PosoffersApi(new \Http\Adapter\Guzzle6\Client());
$id = "id_example"; // string | The id returned from the get operation (GUID)
$key_value = "key_value_example"; // string | Key value, e.g. ssn, externalId, memberNumber, phone number etc.
$contact_type = "contact_type_example"; // string | Contact type, e.g. \"member\" or \"contact\".

try {
    $result = $api_instance->posOfferRedeemByContactTypeAndKey_0($id, $key_value, $contact_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PosoffersApi->posOfferRedeemByContactTypeAndKey_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The id returned from the get operation (GUID) |
 **key_value** | **string**| Key value, e.g. ssn, externalId, memberNumber, phone number etc. |
 **contact_type** | **string**| Contact type, e.g. \&quot;member\&quot; or \&quot;contact\&quot;. |

### Return type

[**\Swagger\Client\Model\RedeemedLoyaltyBarClaimModel**](../Model/RedeemedLoyaltyBarClaimModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

