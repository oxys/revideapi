<?php
/**
 * PersonlookupApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Voyado API V2
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * PersonlookupApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PersonlookupApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation personLookupGetPersonLookup
     *
     * mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.
     *
     * @param  string $social_security_number String that contains socialsecuritynumber. (optional)
     * @param  string $phone_number String that contains phoneNumber. (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Swagger\Client\Model\ContactSearchResult
     */
    public function personLookupGetPersonLookup($social_security_number = null, $phone_number = null)
    {
        list($response) = $this->personLookupGetPersonLookupWithHttpInfo($social_security_number, $phone_number);
        return $response;
    }

    /**
     * Operation personLookupGetPersonLookupWithHttpInfo
     *
     * mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.
     *
     * @param  string $social_security_number String that contains socialsecuritynumber. (optional)
     * @param  string $phone_number String that contains phoneNumber. (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Swagger\Client\Model\ContactSearchResult, HTTP status code, HTTP response headers (array of strings)
     */
    public function personLookupGetPersonLookupWithHttpInfo($social_security_number = null, $phone_number = null)
    {
        $returnType = '\Swagger\Client\Model\ContactSearchResult';
        $request = $this->personLookupGetPersonLookupRequest($social_security_number, $phone_number);

        try {

            try {
                $response = $this->client->send($request);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null, $e->getResponse()->getBody()
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if ($returnType !== 'string') {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\ContactSearchResult',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation personLookupGetPersonLookupAsync
     *
     * mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.
     *
     * @param  string $social_security_number String that contains socialsecuritynumber. (optional)
     * @param  string $phone_number String that contains phoneNumber. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function personLookupGetPersonLookupAsync($social_security_number = null, $phone_number = null)
    {
        return $this->personLookupGetPersonLookupAsyncWithHttpInfo($social_security_number, $phone_number)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation personLookupGetPersonLookupAsyncWithHttpInfo
     *
     * mm              Get a single contactinfomation by eather socialsecuritynumber or mobilephone number.
     *
     * @param  string $social_security_number String that contains socialsecuritynumber. (optional)
     * @param  string $phone_number String that contains phoneNumber. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function personLookupGetPersonLookupAsyncWithHttpInfo($social_security_number = null, $phone_number = null)
    {
        $returnType = '\Swagger\Client\Model\ContactSearchResult';
        $request = $this->personLookupGetPersonLookupRequest($social_security_number, $phone_number);

        return $this->client
            ->sendAsync($request)
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'personLookupGetPersonLookup'
     *
     * @param  string $social_security_number String that contains socialsecuritynumber. (optional)
     * @param  string $phone_number String that contains phoneNumber. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function personLookupGetPersonLookupRequest($social_security_number = null, $phone_number = null)
    {

        $resourcePath = '/api/v2/personlookup/getpersonlookup';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($social_security_number !== null) {
            $queryParams['socialSecurityNumber'] = ObjectSerializer::toQueryValue($social_security_number);
        }
        // query params
        if ($phone_number !== null) {
            $queryParams['phoneNumber'] = ObjectSerializer::toQueryValue($phone_number);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers= $this->headerSelector->selectHeadersForMultipart(
                ['application/json', 'text/json', 'text/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json', 'text/json', 'text/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present

        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];$apiKey = $this->config->getApiKeyWithPrefix('apikey');if ($apiKey !== null) {    $headers['apikey'] = $apiKey;}
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

}
