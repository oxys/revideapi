<?php
/**
 * ContactoverviewApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Voyado API V2
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * ContactoverviewApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ContactoverviewApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation contactOverviewGetContactOverview
     *
     * Get all information about a single contact.
     *
     * @param  string $contact_id contact_id (optional)
     * @param  string $contact_type contact_type (optional)
     * @param  string $email email (optional)
     * @param  string $social_security_number social_security_number (optional)
     * @param  string $mobile_phone mobile_phone (optional)
     * @param  string $custom_key custom_key (optional)
     * @param  string $any any (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return map[string,object]
     */
    public function contactOverviewGetContactOverview($contact_id = null, $contact_type = null, $email = null, $social_security_number = null, $mobile_phone = null, $custom_key = null, $any = null)
    {
        list($response) = $this->contactOverviewGetContactOverviewWithHttpInfo($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any);
        return $response;
    }

    /**
     * Operation contactOverviewGetContactOverviewWithHttpInfo
     *
     * Get all information about a single contact.
     *
     * @param  string $contact_id (optional)
     * @param  string $contact_type (optional)
     * @param  string $email (optional)
     * @param  string $social_security_number (optional)
     * @param  string $mobile_phone (optional)
     * @param  string $custom_key (optional)
     * @param  string $any (optional)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of map[string,object], HTTP status code, HTTP response headers (array of strings)
     */
    public function contactOverviewGetContactOverviewWithHttpInfo($contact_id = null, $contact_type = null, $email = null, $social_security_number = null, $mobile_phone = null, $custom_key = null, $any = null)
    {
        $returnType = 'map[string,object]';
        $request = $this->contactOverviewGetContactOverviewRequest($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any);

        try {

            try {
                $response = $this->client->send($request);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null, $e->getResponse()->getBody()
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if ($returnType !== 'string') {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        'map[string,object]',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation contactOverviewGetContactOverviewAsync
     *
     * Get all information about a single contact.
     *
     * @param  string $contact_id (optional)
     * @param  string $contact_type (optional)
     * @param  string $email (optional)
     * @param  string $social_security_number (optional)
     * @param  string $mobile_phone (optional)
     * @param  string $custom_key (optional)
     * @param  string $any (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function contactOverviewGetContactOverviewAsync($contact_id = null, $contact_type = null, $email = null, $social_security_number = null, $mobile_phone = null, $custom_key = null, $any = null)
    {
        return $this->contactOverviewGetContactOverviewAsyncWithHttpInfo($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation contactOverviewGetContactOverviewAsyncWithHttpInfo
     *
     * Get all information about a single contact.
     *
     * @param  string $contact_id (optional)
     * @param  string $contact_type (optional)
     * @param  string $email (optional)
     * @param  string $social_security_number (optional)
     * @param  string $mobile_phone (optional)
     * @param  string $custom_key (optional)
     * @param  string $any (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function contactOverviewGetContactOverviewAsyncWithHttpInfo($contact_id = null, $contact_type = null, $email = null, $social_security_number = null, $mobile_phone = null, $custom_key = null, $any = null)
    {
        $returnType = 'map[string,object]';
        $request = $this->contactOverviewGetContactOverviewRequest($contact_id, $contact_type, $email, $social_security_number, $mobile_phone, $custom_key, $any);

        return $this->client
            ->sendAsync($request)
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'contactOverviewGetContactOverview'
     *
     * @param  string $contact_id (optional)
     * @param  string $contact_type (optional)
     * @param  string $email (optional)
     * @param  string $social_security_number (optional)
     * @param  string $mobile_phone (optional)
     * @param  string $custom_key (optional)
     * @param  string $any (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function contactOverviewGetContactOverviewRequest($contact_id = null, $contact_type = null, $email = null, $social_security_number = null, $mobile_phone = null, $custom_key = null, $any = null)
    {

        $resourcePath = '/api/v2/contactoverview';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($contact_id !== null) {
            $queryParams['contactId'] = ObjectSerializer::toQueryValue($contact_id);
        }
        // query params
        if ($contact_type !== null) {
            $queryParams['contactType'] = ObjectSerializer::toQueryValue($contact_type);
        }
        // query params
        if ($email !== null) {
            $queryParams['email'] = ObjectSerializer::toQueryValue($email);
        }
        // query params
        if ($social_security_number !== null) {
            $queryParams['socialSecurityNumber'] = ObjectSerializer::toQueryValue($social_security_number);
        }
        // query params
        if ($mobile_phone !== null) {
            $queryParams['mobilePhone'] = ObjectSerializer::toQueryValue($mobile_phone);
        }
        // query params
        if ($custom_key !== null) {
            $queryParams['customKey'] = ObjectSerializer::toQueryValue($custom_key);
        }
        // query params
        if ($any !== null) {
            $queryParams['any'] = ObjectSerializer::toQueryValue($any);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers= $this->headerSelector->selectHeadersForMultipart(
                ['application/json', 'text/json', 'text/xml']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json', 'text/json', 'text/xml'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present

        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];$apiKey = $this->config->getApiKeyWithPrefix('apikey');if ($apiKey !== null) {    $headers['apikey'] = $apiKey;}
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

}
